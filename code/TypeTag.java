package code;

import model.Kind;
import util.Name;

/**
 * Enumerates the nodeType kinds of the CD14 language.
 *
 * @author Jonathon Hope
 */
public enum TypeTag {
    /**
     * The tag of the array nodeType.
     */
    ARRAY("[]"),
    /**
     * The tag of the primitive nodeType: "boolean".
     */
    BOOLEAN("boolean"),
    /**
     * The tag of a container nodeType.
     */
    CONTAINER("container"),
    /**
     * The tag of the primitive nodeType: "int"
     */
    INT("int"),
    /**
     * The tag of the primitive nodeType: "real".
     */
    REAL("real"),
    /**
     * The tag of the primitive nodeType: "void".
     */
    VOID("void"),
    /**
     * The tag of the literal: "string".
     */
    STRING("string"),
    /**
     * An UNKNOWN nodeType.
     */
    ERROR("error");


    TypeTag(String value) {
        this.name = Name.create(value);
    }

    /**
     * The name associated with this tag.
     */
    public final Name name;

    /**
     * @return {@code true} if this kind corresponds to a primitive nodeType.
     */
    public boolean isPrimitive() {
        switch (this) {
            case BOOLEAN:
            case INT:
            case REAL:
            case VOID: // may not be required, since it cannot be exported or declared as a variable.
                return true;
            default:
                return false;
        }
    }

    public Kind getKindLiteral() {
        switch (this) {
            case INT:
                return Kind.INT_LITERAL;
            case REAL:
                return Kind.REAL_LITERAL;
            case BOOLEAN:
                return Kind.BOOLEAN_LITERAL;
            case STRING:
                return Kind.STRING_LITERAL;
            default:
                throw new AssertionError("unknown literal kind " + this);
        }
    }
}
