/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

/**
 * An expression represents an operation on a piece
 * of data and as such results in a <em> value </em>.
 * In any case this interface is merely a
 * <em> marker </em> for an inheritance hierarchy
 * that are all <em> Expressions </em>.
 *
 * @author Jonathon Hope
 */
public interface ExpressionNode extends TreeNode {
}
