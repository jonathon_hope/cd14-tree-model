/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;


import java.util.List;

/**
 * Represents a block of statements, such as those contained by
 * a data, init or function declaration, the body of <em>main</em>
 * or within a loop statement.
 * <pre>
 *     <em>loop</em> startIdentifier:
 *              exit startIdentifier when (condition);
 *
 *         <em>Block Statement</em>
 *
 *     end startIdentifier
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface BlockStatementNode extends StatementNode {

    /**
     * @return a list containing the statements contained
     * by this block.
     */
    List<? extends StatementNode> getStatements();
}
