package model;

import code.TypeTag;

/**
 * Represents a parameter variable declaration, which
 * is punctuated by a unary reference operator. The
 * ReferenceNode is largely symbolic.
 * {@literal
 *      <em> ( int & i ) </em>
 * }
 *
 * @author Jonathon Hope
 * @see model.Kind#ADDRESS_OF
 */
public interface ReferenceTypeNode extends ExpressionNode {

    /**
     * This should allow determination of the primitive or
     * container nature of this {@link ReferenceTypeNode}.
     *
     * @return the {@link code.TypeTag} associated with this
     * reference nodeType.
     */
    TypeTag getTypeTag();
}
