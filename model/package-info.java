/**
 * <h2>CD14 language model</h2>
 *
 * This package contains the <em>data model</em> for the CD14 language.
 *
 * <h2>Root element - TreeNode</h2>
 * This is the basic element of an abstract syntax tree.
 * There are two main hierarchies: Expressions and Statements.
 *
 * <h2>Expressions - ExpressionNode</h2>
 * <em>Expressions</em> are compositions of explicit values, constants,
 * variables, operators, and functions, that when evaluated result
 * in a value of a certain nodeType. In the following example, the value
 * is of nodeType <em>boolean</em>:
 * <pre>
 *      // example expression
 *      (a == b)
 * </pre>
 *
 * <h2>Statements - StatementNode</h2>
 * <em>Statements</em> are sequences of elements that do not result in a
 * value when evaluated. However, they can be evaluated for their
 * <em>side-effects</em>. Note that statements may contain <em>expressions<em>.
 * In the following statement, a conditional expression is evaluated,
 * which may result in the side effect of printing the string "true":
 * <pre>
 *      // example statement
 *      if (a == b) print "true";
 * </pre>
 *
 * <h2>A note regarding CD14 Container Types</h2>
 * The CD14 Language permits container types, which are multi-faceted.
 * They consist of three parts:
 *
 * <h3>id::data - DataNode</h3>
 * Container types may declare a <em>data</em> area, which is of the
 * following form:
 * <pre>
 *      startIdentifier :: data
 *          declare var;
 *      end data
 * </pre>
 * <em>where:</em>
 * <em>startIdentifier</em> is the name of the container.
 *
 * <em>declare var</em> represent one or more variable or constant
 * declarations that are roughly equivalent to member fields in a
 * language like Java.
 *
 * The TreeNode nodeType: <em>DataNode</em> is used to represent this construct.
 *
 * <h3>id::init - InitNode</h3>
 * Container types may declare an <em>init</em> area, which is roughly
 * equivalent to a constructor in a language like java. These have
 * the following form:
 * <pre>
 *      startIdentifier :: init ( params )
 *          var = ...;
 *      end data
 * </pre>
 * <em>where:</em>
 * <em>startIdentifier</em> is the name of the container.
 *
 * <em>params</em> represents a parameter-list, for parameters that may be
 * passed to the initializer, in order to initialise the data
 * variables declared in the id::data area.
 *
 * <em>var = ...</em> represents one or more variable or constant
 * assignments that are roughly equivalent to member fields in a
 * language like Java.
 *
 * The TreeNode nodeType: <em>InitNode</em> is used to represent this construct.
 *
 * <h3>id::func - FuncNode</h3>
 *
 * Container types may declare one or more <em>func</em> procedures, which
 * are roughly equivalent to a member-method in a language like java.
 * These have the following form:
 * <pre>
 *      startIdentifier :: func funcName( params )
 *          block;
 *      end data
 * </pre>
 * <em>where:</em>
 *
 * <em>startIdentifier</em> is the name of the container.
 *
 * <em>params</em>, represents a parameter-list, for parameters that may be
 * passed to the function when it is invoked.
 *
 * <em>block</em> represents a block of executable code, composing one or
 * more statements and/or expressions.
 *
 * The TreeNode nodeType: <em>FuncNode</em> is used to represent this construct.
 *
 * @author Jonathon Hope 
 */
package model;