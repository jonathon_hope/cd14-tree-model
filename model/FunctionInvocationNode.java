package model;

import util.Name;

import java.util.List;

/**
 * Represents the invocation of a particular function.
 *
 * <pre>
 *     <em>startIdentifier</em> ( <em>arguments</em> )
 *
 *     <em>startIdentifier</em> . <em>startIdentifier</em> ( <em>arguments</em> )
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface FunctionInvocationNode extends ExpressionNode {

    /**
     * <em>identifier</em> . name (args)
     *
     * @return the identifier of the container variable used to
     * select/invoke this function.
     */
    Name getIdentifier();

    /**
     * Example:
     * <pre>
     *     identifier "." <em> name </em> ( args )
     *
     *              OR
     *
     *     <em> name </em> ( args )
     * </pre>
     *
     * @return the name of the function being invoked.
     */
    Name getName();

    /**
     * Example:
     * <pre>
     *     id :: program
     *
     *         id :: func  multiply ( int i, int j) returns int
     *              export i * j;
     *         end multiply
     *
     *     id :: main
     *          int i = 45;
     *          int j = 12;
     *
     *          // invoke program level function: arguments: List of (i, j)
     *          multiply (i, j);
     *     id :: end
     *
     * </pre>
     *
     * {@literal
     *      identifier "." name ( <em> args </em> )
     *
     *              OR
     *
     *      name ( <em> args </em>)
     * }
     *
     * @return the arguments as a list of expressions.
     */
    List<? extends ExpressionNode> getArguments();
}
