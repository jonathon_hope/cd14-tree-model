/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

/**
 * This interface defines the Visitor utility used for implementing the
 * <a href="http://en.wikipedia.org/wiki/Visitor_pattern">Visitor Pattern</a>
 * for these Abstract Syntax Tree nodes.
 * <p/>
 * Note: there is one visit method for each TreeNode interface.
 * For example:
 * <p/>
 * <pre>{@literal
 *      R visit<em>TreeNodeType</em>(<em>TreeNodeType</em> node, P param);
 * }</pre>
 *
 * @param <R> the return nodeType of this visitor's methods {@code Void} is
 *            used for signalling that there is no return nodeType.
 * @param <P> the nodeType of the additional parameter to this visitor's
 *            methods.  Use {@code Void} for visitors that do not need an
 *            additional parameter.
 * @author Jonathon Hope
 */
public interface TreeVisitor<R, P> {

    R visitProgram(ProgramNode node, P param);

    R visitContainer(ContainerNode node, P param);

    R visitData(DataNode node, P param);

    R visitFunction(FunctionNode node, P param);

    R visitInit(InitNode node, P param);

    R visitMain(MainNode node, P param);

    R visitLoop(LoopNode node, P param);

    R visitVariable(VariableNode node, P param);

    R visitExit(ExitNode node, P param );

    R visitExport(ExportNode node, P param);

    R visitExpressionStatement(ExpressionStatementNode node, P param);

    R visitBlock(BlockStatementNode node, P param);

    R visitIf(IfNode node, P param);

    R visitInput(InputNode node, P param) ;

    R visitPrint(PrintNode node, P param);

    R visitErrors(ErrorNode node, P param);

    R visitPrimitiveType(PrimitiveTypeNode node, P param);

    R visitLengthAccess(LengthNode node, P param);

    R visitArrayAccess(ArrayAccessNode node, P param);

    R visitArrayDecl(ArrayInitialiserNode node, P param);

    R visitArrayType(ArrayTypeNode node, P param);

    R visitFunctionInvocation(FunctionInvocationNode node, P param);

    R visitAssignment(AssignmentNode node, P param);

    R visitIdentifier(IdentifierNode node, P param);

    R visitInstantiation(InstantiationNode node, P param);

    R visitLiteral(LiteralNode node, P param);

    R visitBinary(BinaryNode node, P param);

    R visitUnary(UnaryNode node, P param);

    R visitTreeNode(TreeNode node, P param);

}
