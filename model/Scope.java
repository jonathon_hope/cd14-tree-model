package model;

import java.util.List;

/**
 * This represents a region of visibility as it pertains
 * to declarations of variables in particular.
 *
 * @author Jonathon Hope
 */
public interface Scope {

    /**
     * @return the enclosing Scope.
     */
    Scope getEnclosingScope();

    /**
     * @return the inner most enclosing function that
     * this Scope is contained within or {@code null} if
     * this Scope pertains to a different region, such as
     * container nodeType.
     */
    FunctionNode getEnclosingFunction();

    /**
     * Example: container member (as part of data structure)
     *
     * @return the container model enclosing this Scope
     * or {@code null} if this Scope pertains to a
     * different region such as a Standalone Function.
     */
    ContainerNode getEnclosingContainer();

    /**
     * @return a list of the elements modeled by the
     * {@link TreeNode} family, that are declared within this Scope.
     */
    List<? extends TreeNode> elements();

}
