/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

/**
 * Examples Statements which contain a Literal:
 * <pre>
 *     int i = 1234567; // an Integer literal
 *
 *     real r = 0.2345; // a Float literal
 *
 *     print "String literal";
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface LiteralNode extends ExpressionNode {
    /**
     * @return the value of this Literal. For literals in CD14, this can only
     * be one of {@link java.lang.Boolean}, {@link java.lang.Float},
     * {@link java.lang.Integer}, or {@link java.lang.String}.
     */
    Object getValue();
}
