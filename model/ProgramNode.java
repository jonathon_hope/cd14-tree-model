package model;

import api.SourceObject;
import util.Name;

import java.util.List;

/**
 * Represents a the highest level of CD14 {@link TreeNode}
 * resulting from a compiler invocation on a SourceObject.
 * <p/>
 * Example:
 * <pre>
 *     <em> startIdentifier </em> :: <em> program </em>
 *
 *          // id :: data definitions
 *          // id :: init definitions
 *          // id :: func definitions
 *
 *     <em> startIdentifier </em> :: <em> main </em>
 *
 *          <em> Block </em>
 *
 *     <em> startIdentifier </em> :: <em> end </em>
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface ProgramNode extends TreeNode {

    /**
     * @return the {@link api.SourceObject} that this
     * program is contained by.
     */
    SourceObject getSourceObject();

    /**
     * @return the startIdentifier for this CD14 program.
     */
    Name getIdentifier();

    /**
     * @return a list containing the Function defined in
     * this program.
     */
    List<? extends TreeNode> getFunctions();

    /**
     * @return a list containing the container data
     * structures defined in this program.
     */
    List<? extends StatementNode> getStructures();

    /**
     * @return a list containing the container initialisers
     * defined in this program.
     */
    List<? extends StatementNode> getInitialisers();

    /**
     * @return the <em> main </em> declaration.
     */
    MainNode getMain();

}
