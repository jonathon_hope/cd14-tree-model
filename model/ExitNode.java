package model;

import util.Name;

/**
 * Represents:
 * {@literal
 *      ExitStatement = [TEXIT TID TWHEN BinaryExpression TSEMI]
 * }
 *
 * @author Jonathon Hope
 */
public interface ExitNode extends StatementNode {

    /**
     * @return the id contained within this exit statement.
     */
    Name getIdentifier();

    /**
     * <pre>
     *     <em>exit startIdentifier when (condition);</em>
     * </pre>
     *
     * @return the expression representing the exit condition.
     */
    ExpressionNode getCondition();

}
