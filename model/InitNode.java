package model;

import util.Name;

import java.util.List;

/**
 * Represents an init declaration.
 * <pre>
 *     <em>startIdentifier</em> :: <em>init</em> ( params )
 *            block;
 *     <em>end</em>
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface InitNode extends StatementNode {

    /**
     * @return the startIdentifier for this <em>initializer</em>.
     */
    Name getIdentifier();

    /**
     * @return the list of variables, expected as params.
     */
    List<? extends VariableNode> getParameters();

    /**
     * @return the statements contained in the body
     * of this <em> init </em> declaration collectively
     * as a {@link model.BlockStatementNode}.
     */
    BlockStatementNode getBody();
}
