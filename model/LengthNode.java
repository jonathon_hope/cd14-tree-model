package model;

import util.Name;

/**
 * {@literal
 *
 *      <em> identifier </em> "." <em> length </em>
 * }
 *
 * @author Jonathon Hope
 */
public interface LengthNode extends ExpressionNode {

    /**
     * @return the identifier, who's length is accessed.
     */
    Name getIdentifier();

}
