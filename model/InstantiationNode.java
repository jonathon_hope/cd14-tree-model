/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

import util.Name;

import java.util.List;

/**
 * Represents an 'init' invocation for a container nodeType.
 * <pre>
 *     identifier "." init "(" args ")"
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface InstantiationNode extends ExpressionNode {

    /**
     * @return the startIdentifier for this instantiation.
     */
    Name getIdentifier();

    /**
     * @return a list of parameters associated with this.
     */
    List<? extends ExpressionNode> getArguments();

}
