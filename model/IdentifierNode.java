/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

import util.Name;

/**
 * </p>
 * The {@code name} represents the <strong>startIdentifier</strong>:
 * <pre>
 *     int <em> name </em>
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface IdentifierNode extends ExpressionNode {

    /**
     * @return the Name of this startIdentifier.
     */
    Name getName();
}
