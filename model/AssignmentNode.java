package model;

/**
 * Represents an assignment expression.
 * <p/>
 * Example:
 * <pre>
 *   <em> variable </em> = <em> expression </em>
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface AssignmentNode extends ExpressionNode {

    /**
     * Note that the CD14 language specification v1.4 is
     * not clear on whether container references can be
     * assigned. For instance:
     *
     * <pre>
     *
     *     example :: program
     *
     *     Cons :: init( )
     *         //...
     *     end init
     *
     *     example :: main
     *
     *         Cons cons1.init();
     *
     *         // is this legal ?
     *         Cons cons2 = & cons1;
     *
     *     example :: end
     *
     * </pre>
     *
     * @return the variable representing the LHS of the
     * assignment operator.
     */
    ExpressionNode getVariable();

    /**
     * @return the RHS expression.
     */
    ExpressionNode getExpression();
}
