package model;

import util.Name;

import java.util.List;

/**
 * This represents a cd14 container data structure.
 * <p/>
 * <pre>
 *     id::data
 *          variable declarations
 *     end data
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface DataNode extends StatementNode {
    /**
     * @return the startIdentifier for this <em>data</em> structure.
     */
    Name getIdentifier();

    /**
     * @return a list of variables declared inside
     * the body of this Data declaration.
     */
    List<? extends VariableNode> getVariables();
}
