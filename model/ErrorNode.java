package model;

import java.util.List;

/**
 * Represents an error within an expression.
 *
 * @author Jonathon Hope
 */
public interface ErrorNode extends TreeNode {

    /**
     * @return the undefined nodes underlying this AST Error.
     */
    List<? extends TreeNode> getErrorNodes();
}
