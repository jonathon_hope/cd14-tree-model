package model;

import util.Name;

/**
 * Represents a variable statement, which can of either
 * PrimitiveType, or ReferenceType.
 * <pre>
 *     <em>Type</em> <em>identifier</em>
 *
 *     <em>Type</em> <em> & </em> <em> identifier </em>
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface VariableNode extends StatementNode {

    /**
     * @return the startIdentifier associated with this variable.
     */
    Name getIdentifier();

    /**
     * @return the Type.
     */
    TreeNode getNodeType();

    /**
     * @return the {@link model.InitNode} or {@code null}
     * iff {@link #getNodeType()} would return a
     * {@link model.PrimitiveTypeNode}
     */
    ExpressionNode getInitiliser();

}
