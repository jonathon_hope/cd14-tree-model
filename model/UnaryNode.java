package model;

/**
 * Represents a unary expression construct. In CD14, as
 * of specification v1.4: there is only one unary operation
 *
 * <pre>
 *     <em> operator </em> expression ;
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface UnaryNode extends ExpressionNode {
    /**
     * @return the right hand side;
     */
    ExpressionNode getExpression();
}
