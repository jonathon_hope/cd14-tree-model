package model;

/**
 * This is an enumeration of the Kinds of TreeNode.
 *
 * @author Jonathon Hope
 */
public enum Kind {

    /**
     * Represents the root of a CD14 program. Used for
     * instances of {@link model.ProgramNode}.
     */
    PROGRAM,

    /**
     * Represents a container (inclusive of all
     * definitions of: data, init and func).
     * Used for instances of {@link model.ContainerNode}.
     */
    CONTAINER,

    //========================================================
    // statements

    /**
     * Used for instances of {@link model.BlockStatementNode}.
     */
    BLOCK_STATEMENT,

    /**
     * Used for instances of {@link model.DataNode}.
     */
    DATA,

    /**
     * Used for instances of {@link model.ExitNode}.
     */
    EXIT,

    /**
     * Used for instances of {@link model.ExportNode}.
     */
    EXPORT,

    /**
     * Used for instances of {@link model.ExpressionStatementNode}.
     */
    EXPRESSION_STATEMENT,

    /**
     * Used for instances of {@link model.FunctionNode}.
     */
    FUNCTION,

    /**
     * Used for instances of {@link model.IfNode}.
     */
    IF,

    /**
     * Used for instances of {@link model.InitNode}.
     */
    INITIALISER,

    /**
     * Used for instances of {@link model.LoopNode}.
     */
    LOOP,

    /**
     * Used for instances of {@link model.MainNode}.
     */
    MAIN,

    /**
     * Used for instances of {@link model.VariableNode}.
     */
    VARIABLE,

    /**
     * Used for instances of {@link model.InputNode}.
     */
    INPUT,

    /**
     * Used for instances of {@link model.PrintNode}.
     */
    PRINT,

    //========================================================
    // expressions

    /**
     * Used for instances of {@link model.ArrayAccessNode}.
     */
    ARRAY_ACCESS,

    /**
     * Used for instances of {@link model.ArrayTypeNode}.
     */
    ARRAY_TYPE,

    /**
     * Used for instances of {@link model.FunctionInvocationNode}
     */
    FUNCTION_CALL,

    /**
     * Used for instances of {@link model.IdentifierNode}.
     */
    IDENTIFIER,

    /**
     * Used for instances of {@link model.InstantiationNode}.
     */
    INSTANTIATION,

    /**
     * Used for instances of {@link model.LengthNode}.
     */
    LENGTH,

    /**
     * Used for instances of {@link model.ContainerTypeNode}.
     */
    CONTAINER_TYPE,

    /**
     * Used for instances of {@link model.PrimitiveTypeNode}.
     */
    PRIMITIVE_TYPE,

    /**
     * Used for instances of {@link model.ReferenceTypeNode}.
     */
    REFERENCE_TYPE,



    //========================================================
    // binary expression operators

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing logical operation {@code and}.
     */
    AND,

    /**
     * Used for instances of {@link model.AssignmentNode}
     * representing the symbol {@code =}.
     */
    ASSIGNMENT,

    /**
     * Used for instances of {@link model.BinaryNode} representing
     * division {@code /}.
     */
    DIVIDE,

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing the symbol {@code ==}.
     */
    EQUAL_TO,

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing the symbol {@code >}.
     */
    GREATER_THAN,

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing the symbol {@code >=}.
     */
    GREATER_THAN_EQUAL,

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing the symbol {@code <}.
     */
    LESS_THAN,

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing the symbol {@code <=}.
     */
    LESS_THAN_EQUAL,

    /**
     * Used for instances of {@link model.BinaryNode} representing
     * multiplication {@code *}.
     */
    MULTIPLY,

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing the symbol {@code !=}.
     */
    NOT_EQUAL_TO,

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing the symbol {@code ^}
     */
    POWER,

    /**
     * Used for instances of {@link model.BinaryNode} representing
     * addition {@code +}.
     */
    PLUS,

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing logical operation {@code or}.
     */
    OR,

    /**
     * Used for instances of {@link model.BinaryNode} representing
     * remainder {@code %}.
     */
    REMAINDER,

    /**
     * Used for instances of {@link model.BinaryNode} representing
     * subtraction {@code -}.
     */
    SUBTRACT,

    /**
     * Used for instances of {@link model.BinaryNode}
     * representing logical operation {@code xor}.
     */
    XOR,

    //========================================================
    // unary expressions

    /**
     * Used for instances of {@link model.UnaryNode} representing
     * the reference operator {@code &}.
     */
    ADDRESS_OF,

    /**
     * Used for instances of {@link model.UnaryNode} representing
     * logical complement operator {@code not}.
     */
    NOT,


    //========================================================
    // literals (also expressions)

   /**
    *
    */
    LITERAL,

    /**
     * Used for instances of {@link model.LiteralNode} representing
     * an integral literal expression of nodeType {@code int}.
     */
    INT_LITERAL,

    /**
     * Used for instances of {@link model.LiteralNode} representing
     * a floating-point literal expression of nodeType {@code float}.
     */
    REAL_LITERAL,

    /**
     * Used for instances of {@link model.LiteralNode} representing
     * a boolean literal expression of nodeType {@code boolean}.
     */
    BOOLEAN_LITERAL,

    /**
     * Used for instances of {@link model.LiteralNode} representing
     * a string literal expression of nodeType {@link String}.
     */
    STRING_LITERAL,

    /**
     * Used for instances of {@link model.ErrorNode}.
     */
    ERROR,

}
