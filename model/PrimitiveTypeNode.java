/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

import code.TypeTag;

/**
 * Represents the primitive types: int | real | boolean.
 *
 * @author Jonathon Hope
 */
public interface PrimitiveTypeNode extends ExpressionNode {

    /**
     * This should return a TypeTag that would return {@code true}
     * for a subsequent call to {@link code.TypeTag#isPrimitive()}
     *
     * @return the {@link code.TypeTag} associated with this
     * primitive nodeType.
     */
    TypeTag getPrimitiveTypeTag();
}