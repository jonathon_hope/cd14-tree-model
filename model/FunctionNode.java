/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

import util.Name;

import java.util.List;

/**
 * <pre>
 *     startIdentifier :: func functionName( params ) returns nodeType
 *         block;
 *     end functionName
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface FunctionNode extends StatementNode {

    public enum Level {
        /**
         * If a function is declared with the program startIdentifier.
         */
        PROGRAM,
        /**
         * If a function is declared with a separate startIdentifier,
         * this is a marker that the function belongs to a
         * container nodeType.
         */
        CONTAINER
    }

    /**
     * @return the {@link model.FunctionNode.Level} of this
     * FunctionNode.
     */
    Level getLevel();

    /**
     * @return the function name.
     */
    Name getName();

    /**
     * This is either the same as {@link ProgramNode#getIdentifier()}
     * or as a {@link ContainerNode#getName()}
     *
     * @return the startIdentifier name.
     */
    Name getIdentifier();

    /**
     * @return the primitive nodeType exported.
     */
    PrimitiveTypeNode getReturnsType();

    /**
     * @return the list of variables, expected as params.
     */
    List<? extends VariableNode> getParameters();

    /**
     * @return the statements contained in the body
     * of this Function declaration collectively as a
     * {@link model.BlockStatementNode}
     */
    BlockStatementNode getBody();
}
