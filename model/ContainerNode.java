package model;

import util.Name;

import java.util.List;

/**
 * Represents the high level of a Container. This can be used
 * to hold all the Nodes relevant to a particular Container.
 * <p/>
 * <pre>
 *     id :: data
 *        //...
 *     end data
 *
 *     id :: init ( params )
 *        //...
 *     end init
 *
 *     id :: func  name ( params )
 *        //...
 *     end name
 *
 *     // other func definitions
 * </pre>
 * <p/>
 * In terms of an instance of this interface representing the
 * above example:
 * <ul>
 *    <li> <em>id</em> would be accessed by
 *         calling {@link #getName()}
 *    </li>
 *    <li> <em>data</em> declaration would be accessed by
 *         calling {@link #getDataDeclaration()}
 *    </li>
 *    <li> A list containing each <em>func</em> definition
 *         would be accessed by calling {@link #getFunctions()}
 *    </li>
 * </ul>
 *
 * @author Jonathon Hope
 */
public interface ContainerNode extends TreeNode {

    /**
     * @return the startIdentifier for this container nodeType.
     */
    Name getName();

    /**
     * @return the {@link model.DataNode} representing the data
     * structure of this container.
     */
    DataNode getDataDeclaration();

    /**
     * @return a list of {@link model.FunctionNode} representing
     * the <em>func</em> definitions associated with the container
     * startIdentifier.
     */
    List<? extends FunctionNode> getFunctions();

    /**
     * @return the {@link model.InitNode} representing the
     * <em>init</em> structure of this container.
     */
    InitNode getInitiliser();
}
