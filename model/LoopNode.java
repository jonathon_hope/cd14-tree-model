/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

import util.Name;

/**
 * A loop statement.
 * <p/>
 * {@literal
 *     LoopStatement = [TLOOP TID TCOLN] [Statement] [TEND TID]
 * }
 *
 * @author Jonathon Hope
 */
public interface LoopNode extends StatementNode {

    /**
     * @return the identifier
     */
    public Name getIdentifier();

    /**
     * @return a Block containing the body.
     */
    BlockStatementNode getStatement();
}
