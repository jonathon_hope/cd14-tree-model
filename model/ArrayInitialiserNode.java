package model;

import util.Name;

/**
 * Represents the bracket initializer for an array variable.
 * {@literal
 *      ArrayType  <em> identifier [ LengthExpression ] </em>
 * }
 *
 * @author Jonathon Hope
 */
public interface ArrayInitialiserNode extends VariableNode {

    /**
     * @return the identifier for this array.
     */
    Name getIdentifier();

    /**
     * @return the length expression for this array.
     */
    ExpressionNode getLength();

}
