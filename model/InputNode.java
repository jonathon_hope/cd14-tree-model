package model;

import java.util.List;

/**
 * Represents an 'input' statement in CD14. This is
 * similar to a Unary Operation.
 * <p/>
 * <pre>
 *     <iostat>	:= 	input <var>, <var-list>;
 *     <var-list>	:= 	<var>, empty;
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface InputNode extends StatementNode {

    /**
     * @return a list of expressions that are the variables
     * to be assigned via input.
     */
    List<? extends ExpressionNode> getVariables();
}
