package model;

/**
 * This represents a cd14 Array nodeType.
 * <p/>
 * <pre>
 *     Type []
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface ArrayTypeNode extends TreeNode {
    /**
     * Arrays can only be of primitive types (int | real | boolean).
     *
     * @return the Type of this array.
     */
    PrimitiveTypeNode getType();
}
