package model;

/**
 * A TreeNode representing an 'export' statement.
 * <pre>
 *     <em>export expression;</em>
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface ExportNode extends StatementNode {

    /**
     * @return the expression being exported.
     */
    ExpressionNode getExpression();
}
