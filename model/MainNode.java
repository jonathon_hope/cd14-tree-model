package model;

import util.Name;

/**
 * Represents the main declaration.
 * <pre>
 *     <em> startIdentifier </em> :: <em> main </em>
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface MainNode extends StatementNode {

    /**
     * @return the start Identifier associated with the
     * declaration of this main statement.
     */
    Name getStartIdentifier();

    /**
     * @return the statement block containing the
     * body of this main statement.
     */
    BlockStatementNode getBody();

    /**
     * The identifier that occurs at the end of the
     * main body (and also the CD14 program).
     *
     * @return the ending identifier associated with the
     * declaration of this main statement.
     */
    Name getEndIdentifier();

}
