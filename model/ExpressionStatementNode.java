package model;

/**
 * This represents an expression statement.
 * Example: such as a standalone function invocation.
 * <p/>
 * {@literal
 *     function();
 * }
 * <p/>
 * Also can be useful for parsing malformed expressions
 * as Statements, allowing determination of a more
 * accurate diagnostic.
 * <p/>
 * Defined as:
 * <pre>
 *     <em>expression</em> ;
 * </pre>
 *
 * @author Jonathon Hope
 * @see model.ErrorNode
 * @see model.AssignmentNode
 * @see model.FunctionInvocationNode
 */
public interface ExpressionStatementNode extends StatementNode {

    /**
     * Use {@link #getKind()} to determine the kind of expression.
     *
     * @return the expression associated with this statement.
     */
    ExpressionNode getExpression();
}
