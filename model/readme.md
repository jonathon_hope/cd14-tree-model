# CD14 language model

This package contains the **data model** for the CD14 language.

## Super interface - TreeNode
This is the basic element of an abstract syntax tree. 
There are two main hierarchies: Expressions and Statements.

## ProgramNode 
Each CD14 program has one ProgramNode that represents the root
of the AST.

## Expressions - ExpressionNode
**Expressions**  are compositions of explicit values, constants, 
variables, operators, and functions, that when evaluated result
in a value of a certain nodeType. In the following example, the value
is of nodeType **boolean**:
    
    // example expression
    (a == b)

## Statements - StatementNode
**Statements**  are sequences of elements that do not result in a
value when evaluated. However, they can be evaluated for their 
**side-effects** Note that statements may contain **expressions**.
In the following statement, a conditional expression is evaluated,
which may result in the side effect of printing the string *"true"*.
    
    // example statement
    if (a == b) print "true";

## A note regarding CD14 Container Types
The CD14 Language permits container types, which are multi-faceted.
They consist of three parts: 

### id::data - DataNode
Container types may declare a **data** area, which is of the 
following form:

    identifier :: data
        declare var;
    end data

*where*:

**identifier** is the name of the container.

**declare var** represent one or more variable or constant 
declarations that are roughly equivalent to member fields in a
language like Java.

The TreeNode nodeType: **DataNode** is used to represent this construct.

### id::init - InitNode
Container types may declare an **init** area, which is roughly
equivalent to a constructor in a language like java. These have
the following form:

    identifier :: init ( params ) 
        var = ...;
    end data

*where*:

**identifier** is the name of the container.

**params** represents a parameter-list, for parameters that may be 
passed to the initializer, in order to initialise the data 
variables declared in the id::data area.

**var = ...** represents one or more variable or constant 
assignments that are roughly equivalent to member fields in a
language like Java.

The TreeNode nodeType: **InitNode** is used to represent this construct.

### id::func - FunctionNode
Container types may declare one or more **func** procedures, which 
are roughly equivalent to a member-method in a language like java.
These have the following form:

    identifier :: func funcName( params ) 
        block;
    end data

*where*:

**identifier** is the name of the container.

**params** represents a parameter-list, for parameters that may be 
passed to the function when it is invoked.

**block** represents a block of executable code, composing one or 
more statements and/or expressions. 

The TreeNode nodeType: **FunctionNode** is used to represent this construct.
