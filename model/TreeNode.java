/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

import util.Position;

/**
 * This interface allowed me to conceptualise the language model.
 * Each significant element of the language is represented as a
 * sub-nodeType of this interface.
 * <p/>
 * The inheritance hierarchy is divided into two groups of TreeNode:
 * <em>Expressions</em>, those which extend {@link model.ExpressionNode}
 * and <em>Statements</em>, those which extend {@link model.StatementNode}.
 *
 * @author Jonathon Hope
 */
public interface TreeNode extends Position {

    /**
     * @return the kind of
     */
    Kind getKind();

    /**
     * Accept method used to implement the visitor pattern. The
     * visitor pattern is used to implement operations on trees.
     * Each TreeNode implementation should return a delegated
     * call to the appropriate method of the {@param treeVisitor}.
     * <p/>
     * Example: An implementation for ArrayAccessNode.
     * {@literal
     * <p/>
     *      <R, P> R accept(TreeVisitor<R, P> treeVisitor, P param) {
     *          return v.visitArrayAccess(this, param);
     *      }
     * }
     *
     * @param <R> result nodeType of this operation.
     * @param <P> additional param (may provide context).
     */
    <R, P> R accept(TreeVisitor<R, P> treeVisitor, P param);

}
