package model;

/**
 * This represents the access of an array index.
 * <pre>
 *     <em> identifier </em> [ <em> index </em> ]
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface ArrayAccessNode extends ExpressionNode {

    /**
     * @return the Identifier.
     */
    IdentifierNode getIdentifier();



    /**
     * As of CD14 Specification v1.4, an array access
     * is defined by:
     * <p/>
     * {@literal
     *
     *      // as right operand
     *      variable> = identifier [ <em> expression </em> ]
     *
     *      OR
     *
     *      // as left operand.
     *      identifier [ <em> expression </em> ] = expression
     *
     *      OR
     *
     *      // as argument
     *      [ init | functionName ] ( identifier [ <em> expression </em> ] )
     * }
     * This implies that the index that an array variable
     * is accessed with can be an expression that is
     * determined at runtime.
     *
     * @return the expression that is used to
     * determine to index.
     */
    ExpressionNode getIndex();
}
