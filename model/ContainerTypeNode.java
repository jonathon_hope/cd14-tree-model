package model;

import util.Name;

/**
 * Represents a container nodeType.
 *
 * {@literal
 *      // ...
 *
 *      Id :: init()
 *      end init
 *
 *      prog :: main
 *          // the nodeType of this var is "Id"
 *          <em> Id var.init() </em>
 *      end :: prog
 * }
 *
 * @author Jonathon Hope
 */
public interface ContainerTypeNode extends ReferenceTypeNode {

    /**
     * @return the Container Type Identifier.
     */
    Name getName();
}
