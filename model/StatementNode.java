/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;

/**
 * This interface is merely a <em> marker </em>
 * for an inheritance hierarchy that are all
 * <em> Statements </em>.
 *
 * @author Jonathon Hope
 */
public interface StatementNode extends TreeNode {
}
