/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package model;


/**
 * Represents the grammar rule:
 * {@literal
 *      IF  :=   if ( BinaryExpr ) Statements else Statements end
 * }
 * </p>
 * Example:
 * <pre>
 *   if ( condition )
 *      thenStatement
 *   end
 *
 *   if ( condition )
 *      thenStatement
 *   else
 *      elseStatement
 *   end
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface IfNode extends StatementNode {
    /**
     * @return the binary expression representing the condition for this if statement.
     */
    ExpressionNode getCondition();

    /**
     * @return the statement sub-tree associated with this if clause.
     */
    StatementNode getThenStatement();

    /**
     * @return null if this if statement has no else branch.
     */
    StatementNode getElseStatement();
}
