package model;


/**
 * A tree node for a binary expression.
 * <p/>
 * For example:
 * <pre>
 *     leftOperand operator rightOperand
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface BinaryNode extends ExpressionNode {
    /**
     * @return the left hand side expression.
     */
    ExpressionNode getLeftOperand();

    /**
     * @return the right hand side expression
     */
    ExpressionNode getRightOperand();
}
