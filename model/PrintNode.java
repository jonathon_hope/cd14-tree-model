package model;

import java.util.List;

/**
 * Represents a 'print' statement.
 * <pre>
 *     <em> print </em> var1, "...", ...;
 *
 *     <em> printline </em> var1, "...", ...;
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface PrintNode extends StatementNode {

    public enum Mode {
        /**
         * For PrintNodes representing the Keyword "print".
         */
        PRINT,
        /**
         * For PrintNodes representing the Keyword "printline".
         */
        PRINTLINE
    }

    /**
     * @return the mode of this PrintNode.
     */
    Mode getMode();

    /**
     * @return a list of expressions that are to be printed.
     */
    List<? extends ExpressionNode> getExpressions();
}
